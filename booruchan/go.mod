module gitlab.com/pointerofnull/booruchan-go/booruchan

go 1.14

require (
	github.com/dghubble/sling v1.3.0
	github.com/go-playground/validator/v10 v10.2.0
)
