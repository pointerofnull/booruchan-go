// Package booruchan wraps various booru apis in golang
package booruchan

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/dghubble/sling"
	"github.com/go-playground/validator/v10"
)

var baseURL = ""

var validate *validator.Validate

// Danbooru json API Struct https://danbooru.donmai.us/post/index.json
type Danbooru []struct {
	HasComments bool   `json:"has_comments"`
	ParentID    int    `json:"parent_id"`
	Status      string `json:"status"`
	HasChildren bool   `json:"has_children"`
	CreatedAt   string `json:"created_at"`
	HasNotes    bool   `json:"has_notes"`
	Rating      string `json:"rating"`
	Author      string `json:"author"`
	CreatorID   int    `json:"creator_id"`
	Width       int    `json:"width"`
	Source      string `json:"source"`
	Score       int    `json:"score"`
	Tags        string `json:"tags"`
	Height      int    `json:"height"`
	FileSize    int    `json:"file_size"`
	ID          int    `json:"id"`
	FileURL     string `json:"file_url"`
	PreviewURL  string `json:"preview_url"`
	Md5         string `json:"md5"`
}

// Yandere json api struct //https://yande.re/post.json
type Yandere []struct {
	ID                  int           `json:"id"`
	Tags                string        `json:"tags"`
	CreatedAt           int           `json:"created_at"`
	UpdatedAt           int           `json:"updated_at"`
	CreatorID           int           `json:"creator_id"`
	ApproverID          interface{}   `json:"approver_id"`
	Author              string        `json:"author"`
	Change              int           `json:"change"`
	Source              string        `json:"source"`
	Score               int           `json:"score"`
	Md5                 string        `json:"md5"`
	FileSize            int           `json:"file_size"`
	FileExt             string        `json:"file_ext"`
	FileURL             string        `json:"file_url"`
	IsShownInIndex      bool          `json:"is_shown_in_index"`
	PreviewURL          string        `json:"preview_url"`
	PreviewWidth        int           `json:"preview_width"`
	PreviewHeight       int           `json:"preview_height"`
	ActualPreviewWidth  int           `json:"actual_preview_width"`
	ActualPreviewHeight int           `json:"actual_preview_height"`
	SampleURL           string        `json:"sample_url"`
	SampleWidth         int           `json:"sample_width"`
	SampleHeight        int           `json:"sample_height"`
	SampleFileSize      int           `json:"sample_file_size"`
	JpegURL             string        `json:"jpeg_url"`
	JpegWidth           int           `json:"jpeg_width"`
	JpegHeight          int           `json:"jpeg_height"`
	JpegFileSize        int           `json:"jpeg_file_size"`
	Rating              string        `json:"rating"`
	IsRatingLocked      bool          `json:"is_rating_locked"`
	HasChildren         bool          `json:"has_children"`
	ParentID            interface{}   `json:"parent_id"`
	Status              string        `json:"status"`
	IsPending           bool          `json:"is_pending"`
	Width               int           `json:"width"`
	Height              int           `json:"height"`
	IsHeld              bool          `json:"is_held"`
	FramesPendingString string        `json:"frames_pending_string"`
	FramesPending       []interface{} `json:"frames_pending"`
	FramesString        string        `json:"frames_string"`
	Frames              []interface{} `json:"frames"`
	IsNoteLocked        bool          `json:"is_note_locked"`
	LastNotedAt         int           `json:"last_noted_at"`
	LastCommentedAt     int           `json:"last_commented_at"`
}

// Konachan json api struct https://konachan.com/post.json?limit=1 https://konachan.net/post.json?limit=1
type Konachan []struct {
	ID                  int           `json:"id"`
	Tags                string        `json:"tags"`
	CreatedAt           int           `json:"created_at"`
	CreatorID           int           `json:"creator_id"`
	Author              string        `json:"author"`
	Change              int           `json:"change"`
	Source              string        `json:"source"`
	Score               int           `json:"score"`
	Md5                 string        `json:"md5"`
	FileSize            int           `json:"file_size"`
	FileURL             string        `json:"file_url"`
	IsShownInIndex      bool          `json:"is_shown_in_index"`
	PreviewURL          string        `json:"preview_url"`
	PreviewWidth        int           `json:"preview_width"`
	PreviewHeight       int           `json:"preview_height"`
	ActualPreviewWidth  int           `json:"actual_preview_width"`
	ActualPreviewHeight int           `json:"actual_preview_height"`
	SampleURL           string        `json:"sample_url"`
	SampleWidth         int           `json:"sample_width"`
	SampleHeight        int           `json:"sample_height"`
	SampleFileSize      int           `json:"sample_file_size"`
	JpegURL             string        `json:"jpeg_url"`
	JpegWidth           int           `json:"jpeg_width"`
	JpegHeight          int           `json:"jpeg_height"`
	JpegFileSize        int           `json:"jpeg_file_size"`
	Rating              string        `json:"rating"`
	HasChildren         bool          `json:"has_children"`
	ParentID            interface{}   `json:"parent_id"`
	Status              string        `json:"status"`
	Width               int           `json:"width"`
	Height              int           `json:"height"`
	IsHeld              bool          `json:"is_held"`
	FramesPendingString string        `json:"frames_pending_string"`
	FramesPending       []interface{} `json:"frames_pending"`
	FramesString        string        `json:"frames_string"`
	Frames              []interface{} `json:"frames"`
}

//Gelbooru json api struct https://gelbooru.com/index.php?page=dapi&s=post&tags=md5%3a2649297cc3c60072ad9c8cfd8b26bb2d&json=1&q=index
//for tags: https://gelbooru.com/index.php?page=dapi&s=tag&tags=md5%3a2649297cc3c60072ad9c8cfd8b26bb2d&json=1&q=index
type Gelbooru []struct {
	Source       string      `json:"source"`
	Directory    string      `json:"directory"`
	Hash         string      `json:"hash"`
	Height       int         `json:"height"`
	ID           int         `json:"id"`
	Image        string      `json:"image"`
	Change       int         `json:"change"`
	Owner        string      `json:"owner"`
	ParentID     interface{} `json:"parent_id"`
	Rating       string      `json:"rating"`
	Sample       int         `json:"sample"`
	SampleHeight int         `json:"sample_height"`
	SampleWidth  int         `json:"sample_width"`
	Score        int         `json:"score"`
	Tags         string      `json:"tags"`
	Width        int         `json:"width"`
	FileURL      string      `json:"file_url"`
	CreatedAt    string      `json:"created_at"`
}

//DanbooruService is results from danbooru using sites
type DanbooruService struct {
	sling *sling.Sling
}

//YandereService is results from yandere
type YandereService struct {
	sling *sling.Sling
}

//KonachanService is results from konachan
type KonachanService struct {
	sling *sling.Sling
}

//GelbooruService is results from gelbooru
type GelbooruService struct {
	sling *sling.Sling
}

// NewDanbooruService returns a new DanbooruService.
func NewDanbooruService(httpClient *http.Client) *DanbooruService {
	return &DanbooruService{
		sling: sling.New().Client(httpClient).Base(baseURL),
	}
}

// NewYandereService returns a new YandereService.
func NewYandereService(httpClient *http.Client) *YandereService {
	return &YandereService{
		sling: sling.New().Client(httpClient).Base(baseURL),
	}
}

// NewKonachanService returns a new KonachanService.
func NewKonachanService(httpClient *http.Client) *KonachanService {
	return &KonachanService{
		sling: sling.New().Client(httpClient).Base(baseURL),
	}
}

// NewGelbooruService returns a new KonachanService.
func NewGelbooruService(httpClient *http.Client) *GelbooruService {
	return &GelbooruService{
		sling: sling.New().Client(httpClient).Base(baseURL),
	}
}

// SearchByMD5 searches Danbooru by md5 and returns results
func (s *DanbooruService) SearchByMD5(md5sum string) (Danbooru, *http.Response, error) {
	results := new(Danbooru)
	var snErr error
	finalURL := fmt.Sprintf("%slimit=1&tags=md5:%s", "https://danbooru.donmai.us/post/index.json?", md5sum)
	//fmt.Printf("FinalURL: %s\n", finalURL)
	resp, err := s.sling.New().Path(finalURL).ReceiveSuccess(results)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("Results: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		fmt.Printf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *results, resp, snErr
}

// SearchByPostID searches danbooru by post id and returns results
func (s *DanbooruService) SearchByPostID(postID int) (Danbooru, *http.Response, error) {
	results := new(Danbooru)
	var snErr error
	finalURL := fmt.Sprintf("%slimit=1&tags=id:%v", "https://danbooru.donmai.us/post/index.json?", postID)
	//fmt.Printf("FinalURL: %s\n", finalURL)
	resp, err := s.sling.New().Path(finalURL).ReceiveSuccess(results)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("Results: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		fmt.Printf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *results, resp, snErr
}

// SearchByMD5 searches yandere by md5 and returns results
func (s *YandereService) SearchByMD5(md5sum string) (Yandere, *http.Response, error) {
	results := new(Yandere)
	var snErr error
	finalURL := fmt.Sprintf("%slimit=1&tags=md5:%s", "https://yande.re/post.json?", md5sum)
	//fmt.Printf("FinalURL: %s\n", finalURL)
	resp, err := s.sling.New().Path(finalURL).ReceiveSuccess(results)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("Results: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		fmt.Printf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *results, resp, snErr
}

// SearchByPostID searches yandere by post id and returns results
func (s *YandereService) SearchByPostID(postID int) (Yandere, *http.Response, error) {
	results := new(Yandere)
	var snErr error
	finalURL := fmt.Sprintf("%slimit=1&tags=id:%v", "https://yande.re/post.json?", postID)
	//fmt.Printf("FinalURL: %s\n", finalURL)
	resp, err := s.sling.New().Path(finalURL).ReceiveSuccess(results)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("Results: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		fmt.Printf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *results, resp, snErr
}

// SearchByMD5 searches konachan.com/net by md5 and returns results
func (s *KonachanService) SearchByMD5(md5sum string, tld string) (Konachan, *http.Response, error) {
	results := new(Konachan)
	var snErr error
	finalURL := fmt.Sprintf("%slimit=1&tags=md5:%s", fmt.Sprintf("https://konachan.%s/post.json?", tld), md5sum)
	//fmt.Printf("FinalURL: %s\n", finalURL)
	resp, err := s.sling.New().Path(finalURL).ReceiveSuccess(results)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("Results: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		fmt.Printf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *results, resp, snErr
}

// SearchByPostID searches konachan by post id and returns results
func (s *KonachanService) SearchByPostID(postID int, tld string) (Konachan, *http.Response, error) {
	results := new(Konachan)
	var snErr error
	finalURL := fmt.Sprintf("%slimit=1&tags=id:%v", fmt.Sprintf("https://konachan.%s/post.json?", tld), postID)
	//fmt.Printf("FinalURL: %s\n", finalURL)
	resp, err := s.sling.New().Path(finalURL).ReceiveSuccess(results)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("Results: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		fmt.Printf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *results, resp, snErr
}

// SearchByMD5 searches gelbooru by md5 and returns results
func (s *GelbooruService) SearchByMD5(md5sum string) (Gelbooru, *http.Response, error) {
	results := new(Gelbooru)
	var snErr error

	validate = validator.New()
	if errs := validate.Var(md5sum, "hexadecimal,required"); errs != nil {
		log.Fatalf("ID: %+v, Val: %+v\n", md5sum, errs)
	}

	finalURL := fmt.Sprintf("%slimit=1&tags=md5:%s", "https://gelbooru.com/index.php?page=dapi&s=post&json=1&q=index&", md5sum)
	//fmt.Printf("FinalURL: %s\n", finalURL)
	resp, err := s.sling.New().Path(finalURL).ReceiveSuccess(results)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("Results: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		fmt.Printf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *results, resp, snErr
}

// SearchByPostID searches gelbooru by post id and returns results
func (s *GelbooruService) SearchByPostID(postID int) (Gelbooru, *http.Response, error) {
	results := new(Gelbooru)
	var snErr error

	validate = validator.New()
	if errs := validate.Var(postID, "numeric,required"); errs != nil {
		log.Fatalf("ID: %+v, Val: %+v\n", postID, errs)
	}
	finalURL := fmt.Sprintf("%slimit=1&id=%v", "https://gelbooru.com/index.php?page=dapi&s=post&json=1&q=index&", postID)
	//fmt.Printf("FinalURL: %s\n", finalURL)
	resp, err := s.sling.New().Path(finalURL).ReceiveSuccess(results)
	if err != nil {
		responseData, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Printf("Error reading response body: %v\n", err)
		}

		fmt.Println("Results: ", string(responseData))
	}
	if resp.StatusCode != 200 {
		fmt.Printf("Query failed with Status Code: %v\n", resp.StatusCode)
	}
	return *results, resp, snErr
}

// Client for various boorus
type Client struct {
	DanbooruService *DanbooruService
	YandereService  *YandereService
	KonachanService *KonachanService
	GelbooruService *GelbooruService
}

// NewClient returns a new Client
func NewClient(httpClient *http.Client) *Client {
	return &Client{
		DanbooruService: NewDanbooruService(httpClient),
		YandereService:  NewYandereService(httpClient),
		KonachanService: NewKonachanService(httpClient),
		GelbooruService: NewGelbooruService(httpClient),
	}
}

//func main() {
//}
